﻿namespace Gateway.GraphQL.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public const string LookUps = "LookUpService";
        public const string Stores = "StoreService";
        public const string Users = "UserService";
        public const string Payment = "PaymentService";
        public const string Carts = "CartService";

        public static IServiceCollection AddHttpClientServices(this IServiceCollection services)
        {
            services.AddHttpClient(Stores, c => c.BaseAddress = new Uri("https://localhost:7162/graphql"));
            services.AddHttpClient(LookUps, c => c.BaseAddress = new Uri("https://localhost:7175/graphql"));
            services.AddHttpClient(Users, c => c.BaseAddress = new Uri("https://localhost:7255/graphql"));

            services
                .AddGraphQLServer()
                .AddRemoteSchema(LookUps)
                .AddRemoteSchema(Users)
                .AddRemoteSchema(Stores);
            return services;
        }
    }
}
