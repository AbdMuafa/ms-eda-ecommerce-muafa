﻿namespace LookUp.Domain.Attribute
{
    public record AttributeStatusChanged(Guid Id, LookUpStatusEnum Status)
    {
        public static AttributeStatusChanged UpdateStatus(Guid id, LookUpStatusEnum status) => new(id, status);
    }
}
