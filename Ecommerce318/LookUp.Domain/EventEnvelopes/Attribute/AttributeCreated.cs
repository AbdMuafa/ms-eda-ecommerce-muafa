﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelope.Attribute
{
    public record AttributeCreated(
        Guid Id,
        AttributeTypeEnum Type,
        string Unit,
        LookUpStatusEnum Status,
        DateTime Modified
        )
    {
        public static AttributeCreated Create(
            Guid id,
            AttributeTypeEnum type,
            string unit,
            LookUpStatusEnum status,
            DateTime Modified
            ) => new(id, type, unit, status, Modified);
    }
}
