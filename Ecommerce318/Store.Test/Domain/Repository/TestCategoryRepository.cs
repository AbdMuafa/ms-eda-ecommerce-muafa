﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;
using Store.Domain.Repositories;
using Store.Test.MockData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Store.Test.Domain.Repository
{
    public class TestCategoryRepository : IDisposable
    {
        protected readonly StoreDbContext _context;
        private readonly ITestOutputHelper _output;

        public TestCategoryRepository(ITestOutputHelper output)
        {
            var options = new DbContextOptionsBuilder<StoreDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

                _context = new StoreDbContext(options);
                _context.Database.EnsureCreated();
                _output = output;
        }

        [Fact]
        public async Task GetAllAsync_ReturnCategoriesCollection()
        {
            _context.Categories.AddRange(CategoryMockData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoriesRepository(_context);
            //Action
            var result = await repo.GetAll();
            var count = CategoryMockData.GetCategories().Count();
            _output.WriteLine("Count : {0}", count);

            //assert
            result.Should().HaveCount(2);
        }
        [Fact]
        public async Task GetByIdAsync_ReturnCategoriesCollection()
        {
            //Arrange
            _context.Categories.AddRange(CategoryMockData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoriesRepository(_context);
            //Action
            var result = await repo.GetById(new Guid("E64434F4-1D7A-4315-AEDC-B1B088CA0BB7"));

            //assert
            result.Name.Should().Be("Ayam Bakar");
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
