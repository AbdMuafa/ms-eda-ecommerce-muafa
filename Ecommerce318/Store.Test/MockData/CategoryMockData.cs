﻿using Store.Domain;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Test.MockData
{
    public class CategoryMockData
    {
        public static List<CategoriesEntity> GetCategories()
        {
            return new List<CategoriesEntity> {
                new CategoriesEntity
                {
                    Id = new Guid("E64434F4-1D7A-4315-AEDC-B1B088CA0BB7"),
                    Name = "Ayam Bakar",
                    Description = "Makanan Utama",
                    Status = StoreStatusEnum.Active
                },
                new CategoriesEntity
                {
                    Id = new Guid("E64434F4-1D7A-4316-AEDC-B1B088CA0BB7"),
                    Name = "Es Kelapa",
                    Description = "Minuman",
                    Status = StoreStatusEnum.Active
                },
                new CategoriesEntity
                {
                    Id = new Guid("E64434F4-1D7A-4317-AEDC-B1B088CA0BB7"),
                    Name = "Es Doger",
                    Description = "Minuman",
                    Status = StoreStatusEnum.Active
                }
            };
        }

    }
}
