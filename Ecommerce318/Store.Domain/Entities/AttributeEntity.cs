﻿namespace Store.Domain.Entities
{
    public class AttributeEntity
    {
        public Guid Id { get; set; }
        public AttributeTypeEnum Type { get; set; }
        public string Unit { get; set; }
        public LookUpStatusEnum Status { get; set; }
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}
