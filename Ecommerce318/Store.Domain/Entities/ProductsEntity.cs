﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Entities
{
    public class ProductsEntity
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public Guid AttributeId { get; set; }
        public Guid? GalleryId { get; set; }
        public string Sku { get; set; } = default!;
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public int Sold { get; set; }
        public int Stock { get; set; }
        public StoreStatusEnum Status { get; set; }

        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("CategoryId")]
        public virtual CategoriesEntity Category { get; set; }

        [ForeignKey("AttributeId")]
        public virtual AttributeEntity Attribute { get; set; }

        [ForeignKey("GalleryId")]
        public virtual GalleryEntity Gallery { get; set; }
    }
}
