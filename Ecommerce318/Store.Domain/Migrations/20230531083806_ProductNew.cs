﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Store.Domain.Migrations
{
    /// <inheritdoc />
    public partial class ProductNew : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_AttributeEntity_AttributeId",
                table: "Products");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AttributeEntity",
                table: "AttributeEntity");

            migrationBuilder.RenameTable(
                name: "AttributeEntity",
                newName: "Attributes");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Attributes",
                table: "Attributes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Attributes_AttributeId",
                table: "Products",
                column: "AttributeId",
                principalTable: "Attributes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Attributes_AttributeId",
                table: "Products");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Attributes",
                table: "Attributes");

            migrationBuilder.RenameTable(
                name: "Attributes",
                newName: "AttributeEntity");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AttributeEntity",
                table: "AttributeEntity",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_AttributeEntity_AttributeId",
                table: "Products",
                column: "AttributeId",
                principalTable: "AttributeEntity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
