﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repositories;

namespace Store.Domain.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> All();
        Task<ProductDto> GetProductById(Guid id);
        Task<ProductDto> AddProduct(ProductDto dto);
        Task<bool> UpdateProduct(ProductDto dto);
        Task<bool> UpdateStatus(Guid id, StoreStatusEnum status);
        Task<bool> RemoveProduct(Guid id);
    }

    public class ProductServices : IProductService
    {
        private readonly IProductRepository _repository;
        private readonly IMapper _mapper;

        public ProductServices(IProductRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<ProductDto> AddProduct(ProductDto dto)
        {
            if (dto != null)
            {
                dto.Status = StoreStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<ProductsEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return _mapper.Map<ProductDto>(entity);
            }
            return new ProductDto();
        }

        public async Task<IEnumerable<ProductDto>> All()
        {
            return _mapper.Map<IEnumerable<ProductDto>>(await _repository.GetAll());
        }

        public async Task<ProductDto> GetProductById(Guid id)
        {
            return _mapper.Map<ProductDto>(await _repository.GetById(id));
        }

        public async Task<bool> UpdateProduct(ProductDto dto)
        {
            if (dto != null)
            {
                var product = await _repository.GetById(dto.Id);
                dto.Status = product.Status;
                var entity = await _repository.Update(_mapper.Map<ProductsEntity>(dto));
                var result = await _repository.SaveChangesAsync();
                if (product != null)
                {
                    if (result > 0)
                        return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateStatus(Guid id, StoreStatusEnum status)
        {
            var product = await _repository.GetById(id);
            if (product != null)
            {
                product.Status = status;
                var entity = await _repository.Update(product);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return true;
            }

            return false;
        }

        public async Task<bool> RemoveProduct(Guid id)
        {
            if (id != null)
            {
                var product = await _repository.GetById(id);
                product.Status = StoreStatusEnum.Removed;
                _repository.Delete(product);
                var result = await _repository.SaveChangesAsync();
                if (result != null)
                {
                    if (result > 0)
                        return true;
                }
            }
            return false;
        }
    }
}
