﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Services
{
    public interface IGalleryService
    {
        Task<IEnumerable<GalleryDto>> All();
        Task<GalleryDto> GetGalleryById(Guid id);
        Task<GalleryDto> AddGallery(GalleryDto dto);

    }
    public class GalleryService : IGalleryService
    {
        private IGalleryRepository _repository;
        private readonly IMapper _mapper;

        public GalleryService(IGalleryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<GalleryDto> AddGallery(GalleryDto dto)
        {
            if (dto!=null)
            {
                dto.Status = StoreStatusEnum.Active;
                var entity = _mapper.Map<GalleryEntity>(dto);
                await _repository.Add(entity);
                var result = await _repository.SaveChangeAsync();

                if (result > 0)
                {
                    return _mapper.Map<GalleryDto>(entity);
                }
            }
            return dto;
        }

        public async Task<IEnumerable<GalleryDto>> All()
        {
            return _mapper.Map<IEnumerable<GalleryDto>>(await _repository.GetAll());
        }

        public Task<GalleryDto> GetGalleryById(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
