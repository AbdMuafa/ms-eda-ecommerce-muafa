﻿using FluentValidation;
using Store.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Validation
{
    public class CategoryValidator : AbstractValidator<CategoriesDto>
    {
        public CategoryValidator()
        {
            RuleFor( x => x.Name).NotNull().WithMessage("Name Cannot null");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("Description minimum 5 char");
        }
    }
}
