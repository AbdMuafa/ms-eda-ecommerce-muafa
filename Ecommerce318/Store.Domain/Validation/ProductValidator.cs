﻿using FluentValidation;
using Store.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Validation
{
    public class ProductValidator : AbstractValidator<ProductDto>
    {
        public ProductValidator()
        {
            RuleFor(x => x.Sku).MinimumLength(3).WithMessage("Sku minimum 3 char").Matches("^[0-9]*$").WithMessage("Just Number Only!");
            RuleFor(x => x.Name).NotNull().WithMessage("Name Cannot null");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("Description minimum 5 char");
            RuleFor(x => x.Price).GreaterThan(0).WithMessage("Price minimum Rp.1");
            RuleFor(x => x.Stock).GreaterThan(0).WithMessage("Out of Stock");
        }
    }
}
