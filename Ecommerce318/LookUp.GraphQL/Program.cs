using Framework.Kafka;
using LookUp.Domain;
using LookUp.Domain.MapProfile;
using LookUp.Domain.Repositories;
using LookUp.Domain.Services;
using LookUp.GraphQL.Scema.Mutations;
using LookUp.GraphQL.Scema.Queries;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json",
                optional: true, reloadOnChange: true);

    options
        .UseSqlServer(builder.Build()
        .GetSection("ConnectionStrings")
        .GetSection("LookUp_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});



builder.Services.AddControllers();
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddKafkaProducer();
builder.Services
    .AddScoped<Query>()
    .AddScoped<AttributeQuery>()
    .AddScoped<CurrenciesQuery>()

    .AddScoped<AttributeMutation>()
    .AddScoped<CurrenciesMutation>()

    .AddScoped<IAttributeService, AttributeService>()
    .AddScoped<IAttributeRepository, AttributeRepository>()
    .AddScoped<ICurrenciesService, CurrenciesService>()
    .AddScoped<ICurrenciesRepository, CurrenciesRepository>()
    .AddGraphQLServer()

    .AddQueryType<Query>()
    .AddTypeExtension<AttributeQuery>()
    .AddTypeExtension<CurrenciesQuery>()
    .AddType<AttributeQuery>()
    .AddType<CurrenciesQuery>()

    //.AddMutationType(m => m.Name("Mutation"))
    .AddMutationType<Mutation>()
    .AddType<AttributeMutation>()
    .AddType<CurrenciesMutation>()
    .AddAuthorization();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseRouting();

app.UseAuthentication();

app.MapControllers();

app.MapGraphQL();

app.Run();
