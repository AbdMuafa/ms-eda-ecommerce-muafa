﻿using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class CurrenciesQuery
    {
        private readonly ICurrenciesService _services;

        public CurrenciesQuery(ICurrenciesService service)
        {
            _services = service;
        }

        public async Task<IEnumerable<CurrenciesDto>> GetAllCurrenciesAsync()
        {
            IEnumerable<CurrenciesDto> result = await _services.All();
            return result;
        }

        public async Task<CurrenciesDto> GetAllCurrenciesByIdAsync(Guid id)
        {
            CurrenciesDto result = await _services.GetCurrenciesById(id);
            return result;

        }
    }
}
