﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Domain.EventEnvelopes.User
{
    public record UserUpdate(
            Guid Id,
            string Username,
            string Password,
            string Firstname,
            string Lastname,
            string Email,
            UserTypeEnum Type
        )
    {
        public static UserUpdate EditUser(
                Guid id,
                string userName,
                string password,
                string firstName,
                string lastName,
                string email,
                UserTypeEnum type
            ) => new(id, userName, password, firstName, lastName, email, type);
    }
}
