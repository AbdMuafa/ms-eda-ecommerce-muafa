﻿using User.Domain.Dtos;
using User.Domain.Entities;
using AutoMapper;

namespace User.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity To Dto Profile")
        {
            CreateMap<UserEntity, UserDto>();
            CreateMap<UserDto, UserEntity>();

            CreateMap<UserEntity, UserDto>();
            CreateMap<UserEntity, LoginDto>();
        }
    }
}
