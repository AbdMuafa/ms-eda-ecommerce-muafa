﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace User.Domain.Entities.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Username).IsRequired();//maxlength 50
            builder.Property(e => e.Password).HasMaxLength(8).IsRequired();//ganti 100
            builder.Property(e => e.FirstName).HasMaxLength(15).IsRequired();//ganti 50
            builder.Property(e => e.LastName).HasMaxLength(15).IsRequired();//ganti 50
            builder.Property(e => e.Email).HasMaxLength(30).IsRequired();//ganti 100
            builder.Property(e => e.Type).IsRequired();
            builder.Property(e => e.Status).IsRequired();

        }
    }
}
