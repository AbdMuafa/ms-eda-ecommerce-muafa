﻿using User.Domain.Entities.Configuration;
using Microsoft.EntityFrameworkCore;


namespace User.Domain.Entities
{
    public class UserDbContext : DbContext
    {

        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
        {

        }
        public DbSet<UserEntity> Users { get; set; }

        protected void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }


}
