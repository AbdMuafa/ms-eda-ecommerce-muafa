using Framework.Kafka;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Net.Http;
using System.Text;
using Store.Domain;
using Store.Domain.MapProfile;
using Store.Domain.Repositories;
using Store.Domain.Services;
using Store.GraphQl.Scema.Mutations;
using Store.GraphQl.Scema.Queries;
using Microsoft.Extensions.FileProviders;
using Store.GraphQl.Scema.Mutation;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Store_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
    .AddJwtBearer("Bearer", opt =>
    {
        var Configuration = builder.Configuration;
        opt.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = Configuration["JWT:ValidateIssuer"],
            ValidAudience = Configuration["JWT:ValidateAudience"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
        };

        opt.Events = new JwtBearerEvents
        {
            OnChallenge = context =>
            {
                context.Response.OnStarting(async () =>
                {
                    await context.Response.WriteAsync("Account not authorized");
                });
                return Task.CompletedTask;
            },
            OnForbidden = context =>
            {
                context.Response.OnStarting(async () =>
                {
                    await context.Response.WriteAsync("Account forbidden");
                });
                return Task.CompletedTask;
            }
        };
    });

builder.Services.AddKafkaProducer();

builder.Services
    .AddScoped<CategoriesQuery>()
    .AddScoped<ProductQuery>()
    .AddScoped<CategoryMutation>()
    .AddScoped<ProductMutation>()
    .AddScoped<GalleryMutation>()
    .AddScoped<ICategoriesRepository, CategoriesRepository>()
    .AddScoped<ICategoriesService, CategoriesService>()
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductService, ProductServices>()
    .AddScoped<IGalleryRepository, GalleryRepository>()
    .AddScoped<IGalleryService, GalleryService>()
    .AddGraphQLServer()
    
    .AddQueryType(x => x.Name("Query"))
    .AddType<CategoriesQuery>()
    .AddType<ProductQuery>()

    .AddMutationType(x => x.Name("Mutation"))
    .AddType<UploadType>()
    .AddType<CategoryMutation>()
    .AddType<ProductMutation>()
    .AddType<GalleryMutation>()
    .AddAuthorization();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseRouting();

app.UseAuthentication();

app.MapControllers();

app.MapGraphQL();

app.UseFileServer(new FileServerOptions()
{
    FileProvider = new PhysicalFileProvider(
            System.IO.Path.Combine(Directory.GetCurrentDirectory(),@"Resources/images")
        ),
    RequestPath = new PathString("/img"),
    EnableDirectoryBrowsing = true
});

app.Run();
