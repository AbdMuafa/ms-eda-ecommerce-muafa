﻿using HotChocolate.Authorization;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQl.Scema.Queries
{
    [ExtendObjectType("Query")]
    public class ProductQuery
    {
        private readonly IProductService _services;

        public ProductQuery(IProductService services)
        {
            _services = services;
        }

        [Authorize]
        public async Task<IEnumerable<ProductDto>> GetAllProductAsync()
        {
            IEnumerable<ProductDto> result = await _services.All();
            return result;
        }

        public async Task<ProductDto> GetProductByIdAsync(Guid id)
        {
            return await _services.GetProductById(id);
        }
    }
}
