﻿using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQl.Scema.Mutations
{
    [ExtendObjectType("Mutation")]
    public class CategoryMutation
    {
        private readonly ICategoriesService _service;

        public CategoryMutation(ICategoriesService service)
        {
            _service = service;
        }

        public async Task<CategoriesDto> AddCategoryAsync(CategoryTypeInput category)
        {
            CategoriesDto dto = new CategoriesDto();
            dto.Name = category.Name;
            dto.Description = category.Description;
            var result = await _service.AddCategories(dto);
            return result;
        }

        public async Task<CategoriesDto> EditCategoryAsync(Guid id, CategoryTypeInput category)
        {
            CategoriesDto dto = new CategoriesDto();
            dto.Id = id;
            dto.Name = category.Name;
            dto.Description = category.Description;
            var result = await _service.UpdateCategories(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Category not found, 404"));
            }
            return dto;
        }

        
        public async Task<CategoriesDto> EditStatusCategoryAsync(Guid id, StoreStatusEnum status)
        {
            var result = await _service.UpdateStatus(id, status);
            if (!result)
            {
                throw new GraphQLException(new Error("Category not found, 404"));
            }
            return await _service.GetCategoriesById(id);
        }

    }
}
