﻿namespace Store.GraphQl.Scema.Mutation
{
    public class GalleryTypeInput
    {
        public string Name { get; set; } = default!;
        public string Description { get; set; } = default!;

        [GraphQLType(typeof(NonNullType<UploadType>))]
        public IFile File { get; set; } = default!;
    }
}
